# Temat Projektu
Application (Java) demonstrating HTTPS client authentication (1 person)
# Przegląd
- Aplikacja Java(Spring Boot) z wbudowanym serwerem servletów Tomcat, bazą danych H2 i silnikiem szablonów ThymeLeaf
- 1 endpoint `https://localhost:9443/user` wyświetlający zalogowanego użytkownika
- Autentykacja poprzez X.509 (Mutual Authentication)
- W folderze `store` znajduje się root certificate i client certificates dla usera "konrad"
- Po autentyfikacji są pobierane uprawnienia z bazy
# Budowanie, uruchamianie
## Wymagania
- Java 11
- Gradle nie jest wymagane (załączony wrapper `gradlew` dla unix, `gradlew.bat` dla windows)
## Budowanie
- Unix `./gradlew build`
- Windows `gradlew.bat build`
- Wynikiem jest zbudowanie, uruchomienie testów i złożenie wykonywalnego JARa z załączonym serwerem Tomcat
## Uruchamianie
- Unix `./gradlew bootRun`
- Windows `gradlew.bat bootRun`
- Po uruchomieniu serwera (W logach napis `Started [...] in [...] seconds (JVM running for [...])`) 
można uruchomić w przeglądarce aplikacje (Testowane na FireFox)
- Przy wpisaniu `localhost:9443/user` Powinnien wystąpić błąd
- Przy wpisaniu `https://localhost:9443/user` Powinno zapytać o certyfikat

# Open SSL
Wszystkie hasła "ASK2020", poza certyfikatem użytkownika "konrad", który ma hasło "Konrad2020ASK"
## Lista poleceń użytych - Serwer
### Root CA (Self signed)
`openssl req -x509 -sha256 -days 3650 -newkey rsa:4096 -keyout rootCA.key -out rootCA.crt` 
### Certyfikat serwera - CSR
`openssl genrsa -des3 -out localhost.key 4096 (ASK 2020)`
`openssl req -new -key localhost.key -out localhost.csr`
### Podpisanie CSR przez rootCA
`openssl x509 -req -CA rootCA.crt -CAkey rootCA.key -in localhost.csr -out localhost.crt -days 365 -CAcreateserial -extfile localhost.ext`
### Eksport do PKCS 12
`openssl pkcs12 -export -out localhost.p12 -name "localhost" -inkey localhost.key -in localhost.crt`
### Zaimportowanie certyfikatu serwera do keystore
`keytool -importkeystore -srckeystore localhost.p12 -srcstoretype PKCS12 -destkeystore keystore.jks -deststoretype JKS`
### Utworzenie truststore
`keytool -import -trustcacerts -noprompt -alias ca -ext san=dns:localhost,ip:127.0.0.1 -file rootCA.crt -keystore truststore.jks`
## Lista poleceń użytych - Klienci
### Utworzenie CSR
`openssl req -new -newkey rsa:4096 -nodes -keyout konrad.key –out konrad.csr`
### Podpisanie CSR przez CA
`openssl x509 -req -CA rootCA.crt -CAkey rootCA.key -in konrad.csr -out konrad.crt -days 365 -CAcreateserial`
### Eksport do pkcs12
`openssl pkcs12 -export -out konrad.p12 -name "clientKonrad" -inkey konrad.key -in konrad.crt`