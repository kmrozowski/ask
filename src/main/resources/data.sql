DROP TABLE IF EXISTS app_user CASCADE;
DROP TABLE IF EXISTS role_privileges CASCADE;
DROP TABLE IF EXISTS app_role CASCADE;
DROP TABLE IF EXISTS app_privilege CASCADE;

CREATE TABLE app_privilege
(
    id   IDENTITY PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE app_role
(
    id   IDENTITY PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE role_privileges
(
    role_id INT,
    privilege_id INT,

    FOREIGN KEY (role_id) REFERENCES app_role(id),
    FOREIGN KEY (privilege_id) REFERENCES app_privilege(id)
);

CREATE TABLE app_user
(
    id IDENTITY PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    role_id INT,
    FOREIGN KEY (role_id) REFERENCES app_role (id)
);

INSERT INTO app_privilege (id, name) VALUES (1, 'READ');
INSERT INTO app_privilege (id, name) VALUES (2, 'WRITE');

INSERT INTO app_role (id, name) VALUES (1, 'ADMIN');
INSERT INTO app_role (id, name) VALUES (2, 'USER');

INSERT INTO role_privileges (Role_id, privilege_id) VALUES (1, 1);
INSERT INTO role_privileges (Role_id, privilege_id) VALUES (1, 2);
INSERT INTO role_privileges (Role_id, privilege_id) VALUES (2, 1);

INSERT INTO app_user (id, username, role_id) VALUES (1, 'ADMIN', 1);
INSERT INTO app_user (id, username, role_id) VALUES (2, 'konrad',2);