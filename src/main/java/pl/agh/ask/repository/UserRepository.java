package pl.agh.ask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.ask.model.AppUser;

import java.util.Optional;

public interface UserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByUsername(String username);
}
