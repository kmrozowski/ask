package pl.agh.ask.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@Controller
@Slf4j
public class HelloController {
    private static final String VIEW_NAME = "hello";

    @GetMapping("/hello")
    public String user(Model model, HttpServletRequest servletRequest) throws CertificateException, FileNotFoundException {
        X509Certificate[] certs = (X509Certificate[]) servletRequest.getAttribute("javax.servlet.request.X509Certificate");

        if (certs != null && certs.length > 0) {
            X509Certificate cert = certs[0];
            model.addAttribute("who", cert.getSubjectX500Principal().getName());
            model.addAttribute("signedByCA", signedByOurCA(cert));
        }

        return VIEW_NAME;
    }

    private boolean signedByOurCA(X509Certificate cert) throws CertificateException, FileNotFoundException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        FileInputStream finStream = new FileInputStream("store/rootCA.crt");
        X509Certificate caCertificate = (X509Certificate) cf.generateCertificate(finStream);

        try {
            cert.verify(caCertificate.getPublicKey());
            return true;
        } catch (Exception e) {
            log.error("Verification error",e);
            return false;
        }
    }
}
