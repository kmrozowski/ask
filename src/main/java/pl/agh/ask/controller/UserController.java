package pl.agh.ask.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
@Slf4j
public class UserController {
    private static final String VIEW_NAME = "user";

    @GetMapping("/user")
    public String user(Model model, Principal principal) {
        log.info("Getting user info for user {}", principal.getName());
        UserDetails currentUser = (UserDetails) ((Authentication) principal).getPrincipal();
        model.addAttribute("username", currentUser.getUsername());
        return VIEW_NAME;
    }
}
