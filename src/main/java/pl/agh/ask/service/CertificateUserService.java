package pl.agh.ask.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.agh.ask.model.Principal;
import pl.agh.ask.repository.UserRepository;

@Service
@Slf4j
public class CertificateUserService implements UserDetailsService {
    private final UserRepository userRepository;

    public CertificateUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .map(Principal::new)
                .orElseThrow(() -> {
                    log.info("Username {} not found", username);
                    return new UsernameNotFoundException(username);
                });
    }
}
