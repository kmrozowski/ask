package pl.agh.ask.model;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
@Table(name = "app_user")
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, updatable = false, unique = true)
    private String username;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private AppRole role;

}
