package pl.agh.ask.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class AppPrivilege {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToMany(mappedBy = "privileges")
    private Collection<AppRole> roles;
}
