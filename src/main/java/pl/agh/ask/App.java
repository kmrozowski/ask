package pl.agh.ask;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.agh.ask.repository.UserRepository;

import java.util.stream.Collectors;

@SpringBootApplication
@Slf4j
public class App implements CommandLineRunner {

    @Autowired
    private UserRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        repository.findAll()
                .stream()
                .peek(u -> log.info("Found user {} with role {}", u.getUsername(), u.getRole().getName()))
                .map(u -> u.getUsername())
                .collect(Collectors.toList());
    }
}
